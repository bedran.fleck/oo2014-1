public class Pessoa {

	private String nome;
	private String idade;
	private String telefone;
	private char sexo;
	private String email;
	private String hangout;
	private String endereco;
	private String rg;
	private String cpf;

//Construtor
	public ControlePessoa() {
		this.listaPessoas = new ArrayList<Pessoa> ();
	}

	public Pessoa(String nome){
		this.nome = nome;
	}
	
	public Pessoa(String nome, String telefone){
		this.nome = nome;
		this.telefone = telefone;
	}	

//Metodos
	public void setNome(String nome){
		this.nome = nome;
	}

	public void setIdade(String idade){
		this.idade = idade;
	}

	public void setTelefone(String telefone){
		this.telefone = telefone;
	}

	public void setSexo(char sexo){
		this.sexo = sexo;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public void setHangout(String hangout){
		this.hangout = hangout;
	}

	public void setEndereco(String endereco){
		this.endereco = endereco;
	}

	public void setRg(String rg){
		this.rg = rg;
	}

	public void setCpf(String cpf){
		this.cpf = cpf;
	}

	public String getNome(){
		return nome;
	}

	public String getIdade(){
		return idade;
	}

	public String getTelefone(){
		return telefone;
	}

	public char getSexo(){
		return sexo;
	}

	public String getEmail(){
		return email;
	}

	public String getHangout(){
		return hangout;
	}

	public String getEndereco(){
		return endereco;
	}

	public String getRg(){
		return rg;
	}

	public String getCpf(){
		return cpf;
	}

}
